import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './app-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MainListingComponent } from './components/main-listing/main-listing.component';
import { PokelistEndComponent } from './components/pokelist-end/pokelist-end.component';
import { SearchComponent } from './components/search/search.component';
import { WishComponent } from './components/main-listing/wish/wish.component';
import { CatchingCartComponent } from './components/main-listing/catching-cart/catching-cart.component';

@NgModule({
    declarations: [
        AppComponent,
        MainListingComponent,
        PokelistEndComponent,
        SearchComponent,
        WishComponent,
        CatchingCartComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AngularMaterialModule,
        HttpClientModule,
        FormsModule, ReactiveFormsModule,

        AppRoutingModule,

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
