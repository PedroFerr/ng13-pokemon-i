import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { NavigationEnd, Router } from '@angular/router';
import { Location } from "@angular/common"

import { filter, Observable, Subscription } from 'rxjs';
import { MainListingComponent } from './components/main-listing/main-listing.component';

import { PokedexService } from './services/pokedex.service';

import { Pokedex, Pokemon } from './services/_pokedex.interfaces';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent extends MainListingComponent implements OnInit {
    
    title = 'ng13-pokemon-i';
    override titlePath = '';
    
    numPokemons: number | undefined;

    override pageSize = 50;
	override pageIndex = 0;
	pageSizeOptions = [1, 50, 100, 500];

    callSearch = false;

    constructor(
        private router: Router,
        protected override api: PokedexService,
        protected override location: Location
    ) {
        super(api, location);

        this.router.events
            .pipe(
                filter((event: any) => event instanceof NavigationEnd)
            )
            .subscribe((event: NavigationEnd) => { this.titlePath = event.url; console.log(this.titlePath);} );
    }

    override ngOnInit(): void {
    }

    receiveSearch(data: string[]) {
        console.log(data);
        if(data.length === 1) {
            this.router.navigate([`/pokemon/${data[0]}`]);
            this.callSearch = false;
        }
    }

    numPokemonsTotal(n: number) {
        this.numPokemons = n;
    }

    callSearchFromMainList(off: boolean) {
        this.callSearch = off;
    }

    /**
     * Handles the pagination, on each new page.
     * 
     * @param event - it is from the inside (it will make happen a new page)
     */
    handlePageEvent(event: PageEvent): void {
		// Refresh the global const:
		this.numPokemons = event.length;
		this.pageSize = event.pageSize;
        this.pageIndex = event.pageIndex;
		
        // console.log(event)

        this.pokedex(this.pageSize, this.pageIndex * this.pageSize);
	}

}
