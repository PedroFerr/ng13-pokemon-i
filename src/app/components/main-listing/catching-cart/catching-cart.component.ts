import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common"

import { MainListingComponent } from '../main-listing.component';

import { PokedexService } from 'src/app/services/pokedex.service';
import { Pokemon } from 'src/app/services/_pokedex.interfaces';

@Component({
    selector: 'app-catching-cart',
    templateUrl: '../../main-listing/main-listing.component.html',
    styleUrls: ['../../main-listing/main-listing.component.scss']
})
export class CatchingCartComponent extends MainListingComponent implements OnInit {

    pokemonsAll: Array<Pokemon> | undefined;

    constructor(
        protected override api: PokedexService,
        protected override location: Location
    ) {
        super(api, location);
    }

    override ngOnInit() {

        const data: string | null = localStorage.getItem('cc');

        this.pokemons_subs = this.api.getAllPokemon().subscribe(n => {
            this.pokemonsAll = n.results;
            
            if (typeof data !== 'undefined' && data !== null) {
                this.pokemons = [];
                const curr: Array<{ idPokemon: number, qty: number }> = JSON.parse(data);
                
                for (const iterator of curr) {
                    this.pokemonsAll.forEach(p => {
                        if(p.url === `https://pokeapi.co/api/v2/pokemon/${iterator.idPokemon}/`) {
                            p.qty = iterator.qty
                            this.pokemons?.push(p)
                        }
                    })
                }
                console.log(this.pokemons)
            }
            // else  (typeof data === 'undefined' || data === null)   // is empty! Must catch the first one, to start with...
        })
    }
}
