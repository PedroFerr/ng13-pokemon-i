/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CatchingCartComponent } from './catching-cart.component';

describe('CatchingCartComponent', () => {
  let component: CatchingCartComponent;
  let fixture: ComponentFixture<CatchingCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatchingCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatchingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
