/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MainListingComponent } from './main-listing.component';

describe('MainListingComponent', () => {
  let component: MainListingComponent;
  let fixture: ComponentFixture<MainListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
