import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { Location } from "@angular/common"
import { Observable, Subscription } from 'rxjs';

import { PokedexService } from 'src/app/services/pokedex.service';
import { Pokedex, Pokemon } from 'src/app/services/_pokedex.interfaces';

@Component({
    selector: 'app-main-listing',
    templateUrl: './main-listing.component.html',
    styleUrls: ['./main-listing.component.scss']
})
export class MainListingComponent implements OnInit, OnChanges, OnDestroy {

    imageLoader = true;
    urlImgPokemon = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/`

    // pokemons$: Observable<Pokedex> | undefined;
    pokemons: Array<Pokemon> = [];
    pokemons_subs: Subscription | undefined;
    ledWishList: string[] = [];

    @Input() pageSize = 0;
    @Input() pageIndex = 0;
    @Input() titlePath: string | undefined;

    @Output() numPokemonsEmitter = new EventEmitter<number>();
    @Output() callSearchEmitter = new EventEmitter<boolean>();

    constructor(
        protected api: PokedexService,
        protected location: Location,
    ) {
        this.titlePath = this.location.path();
    }

    ngOnInit() {
    }

    ngOnChanges() {
        this.pokedex(this.pageSize, this.pageIndex * this.pageSize);
    }

    ngOnDestroy(): void {
        if (this.pokemons_subs) {
            this.pokemons_subs.unsubscribe();
        }
    }

    callSearchFromListing(off: boolean) {
        this.callSearchEmitter.emit(off);
    }

    /**
     * Pokemon Index (max elements of "maxOffSet") with offset of "offSet".
     * Defining the Pokemon array limits, along with the TOTAL number of items of ALL the Pokemons,
     * and lightning the ones are ON, on the Wish list (array)
     * 
     * @param maxOffSet 
     * @param offSet 
     */
     pokedex(maxOffSet: number, offSet: number) {
        this.pokemons_subs = this.api.getPaginatedPokemon(maxOffSet, offSet).subscribe(n => {
            this.pokemons = n.results;
            this.numPokemonsEmitter.emit(n.count);
            this.lightLocalStorage(n.count)
        })    
    }

    /**
     * Iluminates the items ONLY storage on 'localStorage' memory.
     * At the begin, all the Pokemons have the led state OFF (just to place them).
     * 
     * @param numPokemons - the number, in TOTAL, of the Pokemons
     */
    lightLocalStorage(numPokemons: number) {
        this.ledWishList = new Array(numPokemons).fill('favorite_border');
        // But some should get light ON - localStorage
        this.getLS()?.forEach(i => this.ledWishList[Number(i) - 1] = 'favorite');
    }

    /**
     * Turns ON/OFF the led of the Pokemon, of the Wish List
     * 
     * @param id - the 'id' of the Pokemon
     */
    toggleWhishList(id: string) {
        const item: string | null = localStorage.getItem("wl");

        if (!item) {
            this.addNumberLS(id);
        } else {
            this.getLS()?.includes(id.toString()) ? this.removeNumberLS(id) : this.addNumberLS(id);
        }

    }

    addNumberLS(number: string): void {
        const curr: string[] = this.getLS() || [];
        const next: string[] = [...curr, number];
        localStorage.setItem("wl", next.toString());
        // Led is ON
        this.ledWishList[Number(number) - 1] = 'favorite';
    }

    removeNumberLS(number: string): void {
        const curr: string[] = this.getLS() || [];
        const next: string[] = curr.filter(e => e !== number.toString());
        next.length ? localStorage.setItem("wl", next.toString()) : localStorage.removeItem("wl");
        // Turn off the led of this Pokemon:
        this.ledWishList[Number(number) - 1] = 'favorite_border';
    }

    getLS(): string[] | undefined {
        return localStorage.getItem("wl")?.split(',');
    }

    convertNumber(str: string): number {
        return Number(str)
    }
}
