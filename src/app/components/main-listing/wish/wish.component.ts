import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from "@angular/common"

import { MainListingComponent } from '../main-listing.component';

import { PokedexService } from 'src/app/services/pokedex.service';
import { Pokemon } from 'src/app/services/_pokedex.interfaces';


@Component({
    selector: 'app-wish',
    templateUrl: '../../main-listing/main-listing.component.html',
    styleUrls: ['../../main-listing/main-listing.component.scss']
})
export class WishComponent extends MainListingComponent implements OnInit {

    pokemonsAll: Array<Pokemon> | undefined;
    
    constructor(
        protected override api: PokedexService,
        protected override location: Location
    ) {
        super(api, location);
    }

    override ngOnInit() {
        const getLS: string[] | undefined = localStorage.getItem("wl")?.split(',');
        console.log(getLS)

        this.pokemons_subs = this.api.getAllPokemon().subscribe(n => {
            this.pokemonsAll = n.results;
            
            if(getLS) {
                this.pokemons = [];
                for (const iterator of getLS) {
                    this.pokemonsAll.forEach(p => {
                        if(p.url === `https://pokeapi.co/api/v2/pokemon/${iterator}/`) {
                            this.pokemons?.push(p)
                        }
                    })
                }
                console.log(this.pokemons)
            }
        })    


    }
}
