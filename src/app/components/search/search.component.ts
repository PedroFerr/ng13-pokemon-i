import { Component, Input, OnDestroy, AfterViewChecked, OnChanges, OnInit, ChangeDetectorRef, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

import { map, Observable, startWith, Subscription } from 'rxjs';

import { PokedexService } from 'src/app/services/pokedex.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewChecked, OnDestroy {

    name: FormControl = new FormControl('name');
    // name = ''; // For the 'search' (field)...

    arrayPokemons: Array<string> = [];
    pokemons_subs: Subscription | undefined;
    filteredOptions$: Observable<string[]> | undefined;

    @Input() maxOffSet = 0;
    @Input() offSet = 0;

    @Output() serchEmitter = new EventEmitter<string[]>();

    constructor(
        private cdRef: ChangeDetectorRef,
        private api: PokedexService
    ) { }

    ngOnInit() {
        this.initSearch();
    }

    ngOnChanges () {
        this.pokedex(this.maxOffSet, this.offSet * this.maxOffSet)
    }

    ngAfterViewChecked() {
        //explicit change detection to avoid "expression-has-changed-after-it-was-checked-error"
        this.cdRef.detectChanges();
    }

    ngOnDestroy(): void {
        if(this.pokemons_subs) {
            this.pokemons_subs.unsubscribe();
        }
    }

    pokedex(maxOffSet: number, offSet: number) {
        this.arrayPokemons = [];
        this.pokemons_subs = this.api.getPaginatedPokemon(maxOffSet, offSet).subscribe(options => {
            options.results.forEach(e => this.arrayPokemons.push(e.name));
            console.log(this.arrayPokemons)
        })
    }

    initSearch() {
        this.filteredOptions$ = this.name.valueChanges
            .pipe(
                startWith(''),
                map(value => {
                    this.serchEmitter.emit(this._filter(value));
                    return this._filter(value);
                })
            );
    }

    _filter(value: string): string[] {
        const filterValue: string = value.toLowerCase();

        if (filterValue === '') {
            this.name.setValue('');
            return [];
        }
        return this.arrayPokemons.filter(option => option.toLowerCase().includes(filterValue));
    }

    clearSearchField() {
        // this.name = '';
        this.name.setValue('');
        // THIS causes error (ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked) WITHOUT
        // private cdRef: ChangeDetectorRef AND detect changes 'ngAfterViewChecked'
    }

    trackByFn(index: number, option: string) {
        return index; // or 'options.id', unique id corresponding to the item <= ONLY 'options' is Array!!!
    }

}
