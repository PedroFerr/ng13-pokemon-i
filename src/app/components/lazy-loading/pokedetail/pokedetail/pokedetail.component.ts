import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common"

import { Observable, Subscription } from 'rxjs';

import { PokedexService } from 'src/app/services/pokedex.service';
import { Pokedex, Pokemon } from 'src/app/services/_pokedex.interfaces';

interface CatchIt { idPokemon: number, qty: string };

@Component({
    selector: 'app-pokedetail',
    templateUrl: './pokedetail.component.html',
    styleUrls: ['./pokedetail.component.scss']
})
export class PokedetailComponent implements OnInit {

    routeId: string | null = null;

    pokemon$: Observable<Pokemon> | undefined;
    pokemons_subs: Subscription | undefined;

    urlImgPokemon = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/`

    view: any;
    estatistics: Array<{ name: string; value: number; }> = [];

    evt: any;

    currData: Array<CatchIt> = [];
    catchPokemon: CatchIt | undefined;
    // Reactive Form
    formGroup = new FormGroup({
        qtyCatch: new FormControl('1')
    })
    qtyOnInit: string | undefined;
    notExistent: boolean = false;

    constructor(
        public location: Location,
        private route: ActivatedRoute,
        private api: PokedexService
    ) {
        this.evt = innerWidth;
        this.resize(this.evt);
    }

    ngOnInit() {
        this.routeId = this.route.snapshot.paramMap.get('id');
        // Let's find it:
        if (this.routeId !== null) {

            this.pokemon$ = this.api.getPokemon(this.routeId);

            this.pokemons_subs = this.pokemon$.subscribe(pok => {
                // For <ngx-charts-gauge />
                for (const stat of pok.stats) {
                    this.estatistics.push(
                        { name: stat.stat.name, value: stat.base_stat }
                    );
                }
            });
        }

        // Define value for init Qty, on choosen Pokemon
        this.initQty();
    }


    initQty() {

        // this.formGroup.valueChanges.subscribe(value => console.log('formValue changed', value));
        const data: string | null = localStorage.getItem('cc');

        if (typeof data !== 'undefined' && data !== null) {
            this.currData = JSON.parse(data);
            const foundObject: CatchIt | undefined = this.currData.find(p => p.idPokemon === Number(this.routeId));

            if (foundObject) {
                this.formGroup.get('qtyCatch')?.setValue(foundObject.qty);
                this.qtyOnInit = this.formGroup.get('qtyCatch')?.value;
            }
        }
    }

    /**
     * We just add a new Pokemon (id) to 'catching-cart' page - database is another array on 'localStorage'
     * Big time... 1 completely insane day (it's 23:32!) to find out "if (typeof data !== 'undefined' && data !== null) {"
     * 
     * DON'T do
     * ------------------------------------------
     * const data = JSON.parse(localStorage.getItem("cc"))
     * ------------------------------------------
     * NEVER! You'll never get out of it alive...
     * 
     * @param idPokemon the "id" of the Pokemon
     */
    addCatchingCart(catchPokemon: CatchIt): void {
        this.catchPokemon = catchPokemon;
        console.log(catchPokemon)

        const foundObject: CatchIt | undefined = this.currData.find(p => p.idPokemon === catchPokemon.idPokemon);

        if (foundObject) {
            this.foundIt(foundObject)
        } else {
            if (catchPokemon.qty !== '0') {
                const next: Array<CatchIt> = [...this.currData, catchPokemon];
                localStorage.setItem("cc", JSON.stringify(next));
            } else {
                this.notExistent = true;
            }
        }
    }

    foundIt(foundObject: CatchIt) {
        const catchPokemon: CatchIt | undefined = this.catchPokemon
        let next: Array<CatchIt>;

        if(catchPokemon !== undefined) {
            if (catchPokemon.qty !== '0') {
                if (catchPokemon.qty === this.qtyOnInit) {
                    catchPokemon.qty = (Number(catchPokemon.qty) + 1).toString();
                } else {
                    // Qty was changed by user
                    catchPokemon.qty = this.formGroup.get('qtyCatch')?.value;
                }
                Object.assign(foundObject, catchPokemon);
                next = this.currData;

            } else {
                // Delete it, from the 'curr' array:
                next = this.currData.filter(p => p.idPokemon !== catchPokemon.idPokemon)
            }
            localStorage.setItem("cc", JSON.stringify(next));
        }
    }

    /**
     * The graph doesn´t show a natural responsive answer - have to do it by 'hand'.
     * ('setTimeout' is to give time for the chart to be drawn, always)
     * 
     * @param evt - the event triggering 'resize()' - the 'innerWidth', at start, and every time you move the scroll (event.target.innerWidth)
     */
    resize(evt: any) {
        setTimeout(() => {
            this.view = [evt / 2.15, 460];
            if (innerWidth < 767) {
                this.view = [evt / 1.1, 460];
            }
        }, 100);
    }

    onResize(event: any) {
        this.evt = event.target.innerWidth;
        this.resize(this.evt);
    }
}
