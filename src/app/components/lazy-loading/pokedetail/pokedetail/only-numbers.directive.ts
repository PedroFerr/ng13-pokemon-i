import { Directive, ElementRef, forwardRef, HostListener, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
    selector: '[onlyNumbers]',
    providers: [{provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => OnlyNumbersDirective), multi: true}]
})
export class OnlyNumbersDirective implements ControlValueAccessor {
    
    private onChange!: (val: string) => void;
    private onTouched!: () => void;
    private value: string | undefined;

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2
    ) { }

    /**
     *  'filteredValue' can only be numeric, of 'value': value.replace(/[^0-9]/g, '')
     * 
     * @param value - the value of the input
     */
    @HostListener('input', ['$event.target.value'])
    onInputChange(value: string) {
        const filteredValue: string = value.replace(/[^0-9]*/g, '');
        this.updateTextInput(filteredValue, this.value !== filteredValue);
    }

    /**
     * 'onBlur', the input can stay with ''
     */
    @HostListener('blur')
    onBlur() {
        if(!this.value) {
            this.updateTextInput('1', this.value !== '1')
        } else {
            this.onTouched();
        }
    }

    private updateTextInput(value: string, propagateChange: boolean) {
        this.renderer.setProperty(this.elementRef.nativeElement, 'value', value);
        if (propagateChange) {
            this.onChange(value);
        }
        this.value = value;
    }




    // -------------------------------
    // ControlValueAccessor Interface
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled: boolean): void {
        this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', isDisabled);
    }
    writeValue(value: any): void {
        value = value ? String(value) : '';
        this.updateTextInput(value, false);
    }
}
