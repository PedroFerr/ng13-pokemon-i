import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PokedetailComponent } from './pokedetail/pokedetail.component';

const routes: Routes = [
    { path: '', component: PokedetailComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PokedetailRoutingModule { }
