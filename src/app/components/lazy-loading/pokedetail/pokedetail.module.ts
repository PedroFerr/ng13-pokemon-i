import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AngularMaterialModule } from 'src/app/app-material.module';
import { PokedetailRoutingModule } from './pokedetail-routing.module';

import { PokedetailComponent } from './pokedetail/pokedetail.component';
import { OnlyNumbersDirective } from './pokedetail/only-numbers.directive';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
	imports: [
		CommonModule,
		FormsModule, ReactiveFormsModule,
        AngularMaterialModule,
		PokedetailRoutingModule,

		NgxChartsModule
	],
	declarations: [
        PokedetailComponent,
		OnlyNumbersDirective
    ],
	providers: [],
})
export class PokedetailModule { }

