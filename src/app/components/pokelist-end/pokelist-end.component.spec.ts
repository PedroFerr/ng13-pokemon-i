/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PokelistEndComponent } from './pokelist-end.component';

describe('PokelistEndComponent', () => {
  let component: PokelistEndComponent;
  let fixture: ComponentFixture<PokelistEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokelistEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokelistEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
