import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PokelistEndComponent } from './components/pokelist-end/pokelist-end.component';
import { SearchComponent } from './components/search/search.component';
import { WishComponent } from './components/main-listing/wish/wish.component';
import { CatchingCartComponent } from './components/main-listing/catching-cart/catching-cart.component';

const routes: Routes = [

    { path: '', redirectTo: 'pokemon', pathMatch: 'full' },

    { path: 'pokemon', component: PokelistEndComponent },
    { path: 'search', component: SearchComponent },
    { path: 'wish', component: WishComponent },
    { path: 'catching-cart', component: CatchingCartComponent },

    { path: 'pokemon/:id', loadChildren: () => import('./components/lazy-loading/pokedetail/pokedetail.module').then(m => m.PokedetailModule) },

    { path: '**', loadChildren: () => import('./components/lazy-loading/not-found/not-found.module').then(m => m.NotFoundModule) },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
