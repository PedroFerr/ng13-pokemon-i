import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, share } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Pokedex, Pokemon } from './_pokedex.interfaces';

@Injectable({
	providedIn: 'root'
})
export class PokedexService {

	_pokeURI = environment.pokeURI;

	constructor(private _http: HttpClient) { }

	getAllPokemon() {
		return this._http.get<Pokedex>(`${this._pokeURI}pokemon?limit=1118&offset=0`)
	}

	getPaginatedPokemon(maxOffSet: number, offSet: number): Observable<Pokedex> {
        return this._http.get<Pokedex>(`${this._pokeURI}pokemon?limit=${maxOffSet}&offset=${offSet}`)
			.pipe(
				// Get ride of a 2nd call, gettin (1 pair of) duplicated items
				// https://itnext.io/the-magic-of-rxjs-sharing-operators-and-their-differences-3a03d699d255
				//
				// refCount()
				// publish(),
				share()
				//
				// publishReplay(),
				// refCount()
				//
				// shareReplay(2)
				// publishLast()

				// Or collect, and pend, all the requests. When a double is found... delete it
				// tap(() => {this.pending.delete(url);})
				// https://stackoverflow.com/a/57308372/2816279
			);
    }

	getPokemon(id: string):  Observable<Pokemon>  {
        return this._http.get<Pokemon>(`${this._pokeURI}pokemon/${id}`)
    }
}
