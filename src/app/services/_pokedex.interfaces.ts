export interface Pokedex {
    count: number;
    results: Pokemon[];
}


export interface Pokemon {
    name: string;
    url: string;

    abilities: Ability[];
    base_experience: number;
    forms: Api;
    game_indices: GameIndex[];
    height: number;
    held_items: any[];
    id: number;
    is_default: boolean;
    location_area_encounters: string;
    moves: any[];
    order: number;
    species: Api;
    sprites: Sprite;
    stats: any[];
    type_1: string;
    type_2: string;
    weight: number;
    image: string;
    color: string | null;
    types: any[];

    qty: number;
}

export interface Ability {
    ability: Api;
    is_hidden: boolean;
    slot: number;
}
  
export interface GameIndex {
    game_index: number;
    version: Api;
}
  

export interface Api {
    name: string;
    url: string;
}

export interface Sprite {
    back_default: any;
    back_female: any;
    back_shiny: any;
    back_shiny_female: any;
    front_default: string;
    front_female: any;
    front_shiny: any;
    front_shiny_female: any;
    other: {
      'official-artwork': {
        front_default: string;
      };
      dream_world: {
        front_default: string;
      };
    };
  }
  